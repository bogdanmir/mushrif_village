$(document).ready(function() {


    $('#fotorama').fotorama({
        arrowPrev: '<img src="../images/icon-phone.png" width="30" height="30" >',
        arrowNext: '<img src="/path-to-image/Next.png" width="30" height="30" >'
    });

  $('.gallery-colorbox').colorbox({
    // rel: 'album',
    fixed: true,
    maxWidth: '90%',
    maxHeight: '90%',
    current: '',
    title: function() {
      var el = $(this);
      if (el.data('title')) {
        return "<div class='title-wrapper'><div class='title'>" + el.data('title') + "</div><div class='description'>" + el.data('description') + "</div></div>";
      } else {
        return "";
      }
    }
  });
  $('.highlight-colorbox').colorbox({
    rel: 'album',
    fixed: true,
    maxWidth: '90%',
    maxHeight: '90%',
    current: '',
    className: 'no-title'
  });

  $(document).on('cbox_open', function() {
    $(document.body).css('overflow', 'hidden');
  }).on('cbox_closed', function() {
    $(document.body).css('overflow', '');
  });

  $('.preaty-select').each(function() {
    var $this = $(this),
      numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
      'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
      $('<li />', {
        text: $this.children('option').eq(i).text(),
        rel: $this.children('option').eq(i).val()
      }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
      e.stopPropagation();
      $('div.select-styled.active').not(this).each(function() {
        $(this).removeClass('active').next('ul.select-options').hide();
      });
      $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
      e.stopPropagation();
      $styledSelect.text($(this).text()).removeClass('active');
      $this.val($(this).attr('rel'));
      $list.hide();
    });

    $(document).click(function() {
      $styledSelect.removeClass('active');
      $list.hide();
    });

  });
});



// var proximityMap = (function() {
//   if (document.getElementById('js-proximity-map')) {
//     var layer;
//     var mapMinZoom = 4;
//     var mapMaxZoom = 4;
//     var map = L.map('js-proximity-map', {
//       maxZoom: mapMaxZoom,
//       minZoom: mapMinZoom,
//       attributionControl: false,
//       scrollWheelZoom: false,
//       zoomSnap: 1
//       // zoomControl:false
//     }).setView([0, 0], mapMaxZoom);

//     var mapBounds = new L.LatLngBounds(
//         map.unproject([0, 1175], mapMaxZoom),
//         map.unproject([2266, 0], mapMaxZoom));

//     map.fitBounds(mapBounds);

//     layer = L.tileLayer('data/proximity/{z}/{x}/{y}.jpg', {
//       minZoom: mapMinZoom, maxZoom: mapMaxZoom,
//       continuousWorld: 'false',
//       bounds: mapBounds,
//     }).addTo(map);

//   } else {
//     return
//   }
// }())

// var glanceMap = (function() {
//   if (document.getElementById('mapid')) {
//     var layer;
//     var mapMinZoom = 4;
//     var mapMaxZoom = 4;
//     var map = L.map('mapid', {
//       maxZoom: mapMaxZoom,
//       minZoom: mapMinZoom,
//       attributionControl: false,
//       scrollWheelZoom: false,
//       zoomSnap: 1,
//       crs: L.CRS.Simple,
//       // zoomControl:false
//     }).setView([0, 0], mapMaxZoom);

//     var mapBounds = new L.LatLngBounds(
//         map.unproject([0, 1175], mapMaxZoom),
//         map.unproject([2266, 0], mapMaxZoom));

//     map.fitBounds(mapBounds);

//     layer = L.tileLayer('/data/glance/{z}/{x}/{y}.jpg', {
//       minZoom: mapMinZoom, maxZoom: mapMaxZoom,
//       continuousWorld: 'false',
//       bounds: mapBounds,
//     }).addTo(map);
//   }
// }())

var welcomeCarousel = (function() {
  $('.owl-carousel').owlCarousel({
    items: 1,
    nav: true
  });
}())


$("#example_id").ionRangeSlider();
// Elements
var scene = document.querySelector('.section-hero');
var input = document.querySelector('.section-hero');

// Pretty simple huh?
var parallax = new Parallax(scene, {
  hoverOnly: true,
  relativeInput: true,
  inputElement: input,
  scaleX: 3,
  scaleY: 12
});

$(document).ready(function() {
  $('.bxslider').bxSlider({
    pagerCustom: '#bx-pager'
  });
  $('.forOpen').click(function(){
    $('.logo-wrapper').toggleClass("navbar-opened")
  })
});



















$(document).ready(function() {

       // map
/*
* Workaround for 1px lines appearing in some browsers due to fractional transforms
* and resulting anti-aliasing.
* https://github.com/Leaflet/Leaflet/issues/3575
*/
(function(){
var originalInitTile = L.GridLayer.prototype._initTile
L.GridLayer.include({
    _initTile: function (tile) {
        originalInitTile.call(this, tile);

        var tileSize = this.getTileSize();

        tile.style.width = tileSize.x + 1 + 'px';
        tile.style.height = tileSize.y + 1 + 'px';
    }
});
})()
    var mymap = L.map('mapid', {
        attributionControl: false,
        scrollWheelZoom: false,
        minZoom: 11,
        maxZoom: 11,
        zoomSnap: 1,
        zoomControl:false,
    }).setView([2,1.1], 11);

    var corner1 = L.latLng(0.0, 0.0),
        corner2 = L.latLng(-1, 2.107),
         bounds = L.latLngBounds(corner1, corner2);

   mymap.setMaxBounds(bounds);
   mymap.on('drag', function() {
       mymap.panInsideBounds(bounds, { animate: false });
   });

   var GreenIcon = new L.Icon({
    iconUrl: './images/geo-icon.png',
    iconSize:     [31, 31],
    iconAnchor:   [15, 15],
    popupAnchor:  [0, -28]
  });


   var BedroomIcon = new L.Icon({
    iconUrl: './images/bedroom.png',
    iconSize:     [226, 142],
    iconAnchor:   [15, 15]
  });
   var ParkIcon = new L.Icon({
    iconUrl: './images/park.png',
    iconSize:     [128, 142],
    iconAnchor:   [15, 15]
  });
   var ClubhouseIcon = new L.Icon({
    iconUrl: './images/clubhouse.png',
    iconSize:     [172, 142],
    iconAnchor:   [15, 15]
  });
   var ThreeBedroomIcon = new L.Icon({
    iconUrl: './images/3bedroom.png',
    iconSize:     [227, 142],
    iconAnchor:   [15, 15]
  });
   var TwoBedroomIcon = new L.Icon({
    iconUrl: './images/2bedroom.png',
    iconSize:     [301, 142],
    iconAnchor:   [15, 15]
  });

    // create popup contents
   var customOptions =
       {
       'maxWidth': '500',
       'className' : 'custom-popup',
       autoPan: false
       }

   // create marker object, pass custom icon as option, pass content and options to popup, add to map
   // L.marker([-0.4, 0.4], {icon: GreenIcon}).bindPopup("Mozilla Toronto Offices<br/>22",customOptions).addTo(mymap);
   // L.marker([-0.05, 0.4], {icon: GreenIcon}).bindPopup("Mozilla Toronto Offices<br/>",customOptions).addTo(mymap);
   L.marker([-0.48, 0.96], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame<br/>ipsum dolor",customOptions).addTo(mymap);
   L.marker([-0.23, 1.22], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame 2<br/>ipsum dolor",customOptions).addTo(mymap);
   L.marker([-0.55, 1.17], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame 3<br/>ipsum dolor",customOptions).addTo(mymap);
   L.marker([-0.47, 1.34], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame 4<br/>ipsum dolor",customOptions).addTo(mymap);
   L.marker([-0.38, 1.39], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame 5<br/>ipsum dolor",customOptions).addTo(mymap);
   L.marker([-0.32, 1.29], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame 6<br/>ipsum dolor",customOptions).addTo(mymap);
   L.marker([-0.22, 1.68], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame 7",customOptions).addTo(mymap);
   L.marker([-0.41, 1.6], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame 8",customOptions).addTo(mymap);
   L.marker([-0.6, 1.68], {icon: GreenIcon}).bindPopup("Lorem ipsum dolor sit ame 9",customOptions).addTo(mymap);


   L.marker([-0.32, 1.625], {icon: BedroomIcon}).addTo(mymap);
   L.marker([-0.34, 1.26], {icon: ParkIcon}).addTo(mymap);
   L.marker([-0.23, 1.43], {icon: ClubhouseIcon}).addTo(mymap);
   L.marker([-0.52, 0.98], {icon: ThreeBedroomIcon}).addTo(mymap);
   L.marker([-0.25, 0.9], {icon: TwoBedroomIcon}).addTo(mymap);



    // L.tileLayer('./data/{z}/{x}/{y}.jpg' ,{
    L.tileLayer('data/{z}/{x}/{y}.jpg', {
        continuousWorld: 'false',
        bounds: bounds
    }).addTo(mymap);


    /*HGH LATITUDE POPUPS OPENING DOWNWARD instead of UPWARD - prevent yoyo effect*/
    mymap.on('popupopen', function(e) {
        // saving old anchor point X Y
        if(!e.popup.options.oldOffset) e.popup.options.oldOffset = e.popup.options.offset;
        var px = mymap.project(e.popup._latlng);
        var mapMaxY =  mymap.project(corner1).y;

        // we calculate popup content height (jQuery)
        var heightOpeningPopup = $('#mapid').find('.leaflet-popup-content').height() + 60;

        var temp2 = heightOpeningPopup+67;
        if(heightOpeningPopup > px.y-mapMaxY){ // if it will go above the world, we prevent it to do so
            // we make the popup go below the poi instead of above
            e.popup.options.offset = new L.Point(0, temp2);
            // we make the popup tip to be pointing upward (jQuery)
            $('#mapid').addClass("reverse-popup");
            e.popup.update();
        }
        else{ // we allow auto pan if the popup can open in the normal upward way
            e.popup.options.offset = e.popup.options.oldOffset;
            e.popup.options.autoPan = true;
            $('#mapid').removeClass("reverse-popup");
            e.popup.update();
        }
    });

    var panOffset = 250;
    $('.arrow-left').on('click', function(event) {
        event.preventDefault();
    });
    $('.arrow-left').on('mousedown', function(event) {
        event.preventDefault();
        mymap.panBy(new L.Point(-panOffset, 0));
    });

    $('.arrow-right').on('click', function(event) {
        event.preventDefault();
    });
    $('.arrow-right').on('mousedown', function(event) {
        event.preventDefault();
        mymap.panBy(new L.Point(panOffset, 0));
    });

    $('.arrow-up').on('click', function(event) {
        event.preventDefault();
    });
    $('.arrow-up').on('mousedown', function(event) {
        event.preventDefault();
        mymap.panBy(new L.Point(0, -panOffset));
    });

    $('.arrow-down').on('click', function(event) {
        event.preventDefault();
    });
    $('.arrow-down').on('mousedown', function(event) {
        event.preventDefault();
        mymap.panBy(new L.Point(0, panOffset));
    });
    // end: map


});




///////////////////////////////////////////////////////////////////////////////////////
//proxima-map
  $(document).on("click", ".prox-thumb .item", function(){
    $('.prox-thumb .item').removeClass('active');
    $(this).addClass('active');

      $('.prox-box .item').hide();
      var itemAttr = $(this).attr('data-item');
      $('.prox-box .item[data-item = '+itemAttr+']').show();

      var timeAttr = $(this).attr('data-time');
      $('.prox-box .item[data-time = '+timeAttr+']').show();

      var time2Attr = $(this).attr('data-time');
      $('.prox-box .item[data-time2 = '+time2Attr+']').show();

      var time3Attr = $(this).attr('data-time');
      $('.prox-box .item[data-time3 = '+time3Attr+']').show();

      if ($(this).hasClass('all-th')){
        $('.prox-box .item').show();
      }
  });



